//
//  ViewController.swift
//  S
//
//  Created by Lindi on 11/26/16.
//  Copyright © 2016 Tenton. All rights reserved.
//

import UIKit

class FirstController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var listLabel: UILabel!
    @IBOutlet weak var sortButton: UIButton!
    
    // MARK: Variables
    var list : [String]!
    var onSortedFinish : ((_ sortedArray : [String]) -> Void)!
    
    // MARK: UIViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //init values
        list = ["l", "m", "h","v", "f", "x", "a"]
        self.listLabel.text = Helpers.printList(list: list)
    }
    
    // MARK: Functions and IBActions
    
    @IBAction func sortButton_TouchUp(_ sender: UIButton) {
        if let secondVC = self.storyboard!.instantiateViewController(withIdentifier: "SecondController") as? SecondController{
            secondVC.list = list
            self.present(secondVC, animated: true, completion: nil)
            
            //implement onSortedFinish
            secondVC.onSortedFinish = { sortedArray in
                self.listLabel.text = "sorted : " + Helpers.printList(list: sortedArray)
            }
        }
    }
}

