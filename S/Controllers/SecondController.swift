//
//  SecondViewController.swift
//  S
//
//  Created by Lindi on 11/26/16.
//  Copyright © 2016 Tenton. All rights reserved.
//

import UIKit

class SecondController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var sortedArrayLabel: UILabel!
    @IBOutlet weak var doneButton: UIButton!

    
    // MARK: Variables
    var list : [String]!
    var sortedArray : [String]!
    var onSortedFinish : ((_ sortedArray : [String]) -> Void)!
    
    // MARK: UIViewController lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let backgroundQueue = DispatchQueue(label: "com.solaborate.S.sortTh", qos: .background, target: nil)
        
        //execute in separate thread
        backgroundQueue.async {
            let sortedArray = self.sortInAlphabeticalOrder()
            
            // return sorted array and update UI in main thread
            DispatchQueue.main.async {
                self.sortedArray = sortedArray

                self.sortedArrayLabel.text = Helpers.printList(list: sortedArray)
                self.doneButton.isHidden = false
            }
        }
    }
    
    // MARK: Functions and IBActions
    
    private func sortInAlphabeticalOrder() -> [String] {
        return list.sorted()
    }
    
    @IBAction func doneButton_TouchUp(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            self.onSortedFinish!(self.sortedArray)
        })
    }
}
