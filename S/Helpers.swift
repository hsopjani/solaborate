//
//  Helper.swift
//  S
//
//  Created by Lindi on 11/26/16.
//  Copyright © 2016 Tenton. All rights reserved.
//

import UIKit

class Helpers: NSObject {
    static func printList (list : [String]) -> String {
        return list.joined(separator: " ")
    }
}

