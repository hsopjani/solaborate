# README #

An unsorted array of type string which holds the values is given:

##Input: [“l”, "m", "h","v", "f", "x", "a"]##

and displaying result should be: 

## Result: [“a”, "f", "h", "l", "m", "v", "x"] ##

Let’s assume we have Main storyboard, containing two view controllers; FirstController, SecondController.

First controller is initial view controller that will hold a button ‘Sort - which will sort an array and display it in UILabel’. On button tap, will present the SecondController and will do the sort. You can decide in which way you will pass the variables. Meantime, a callback ( completion closure) will be passed to the second controller, which will be called upon SecondController dismissal, a Done button in SecondController. The closure function will take an array of type string (Array<String>) as argument and returns void.  ; (this array will be displayed in FirstController)

Second Controller (SecondController) will contain a private function( sortInAlphabeticalOrder ) that will do a calculation without blocking the main thread (UI) and should return the array, in alphabetical order. This function will sort alphabetically the given input parameter which is an array of type string: Array<String>.  


##*Note: Pay attention to the heap, should be no leaks. You can use any sorting algorithm.##